package ru.soy_demetrio.research.quarkus.services;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import ru.soy_demetrio.research.quarkus.domain.Person;

/**
 * Интерфейс точки доступа к данным физических лиц. 
 * @author soy_demetrio
 */
public interface PersonService {

	/**
	 * Выборка всей таблицы.
	 * @return
	 */
	public Multi<Person> getAll();
	
	/**
	 * Выборка по первичному ключу.
	 * @param argId
	 * @return
	 */
	public Uni<Person> getById(long argId); 
	
	/**
	 * Сохранение: если arg.id == null - вставка, иначе - обновление.
	 * @param arg
	 * @return
	 */
	public Uni<Long> save(Person arg);
	
	/**
	 * Удаление.
	 * @param argId
	 * @return
	 */
	public Uni<Boolean> delete(long argId);
	
}
