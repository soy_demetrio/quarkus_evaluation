# quarkus_evaluation
Исследование Quarkus

## Запуск в отладочном режиме

```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Упаковка и запуск

```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

## Создание платформозависимого исполняемого модуля

```shell script
./mvnw package -Pnative
```
Or, if you don't have GraalVM installed, you can run the native executable build in a container using:
```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```
You can then execute your native executable with: `./target/quarkus_eval-1.0.0-SNAPSHOT-runner`

## Создание Docker-образа

Создание образа:
```shell script
./mvnw clean package -Dquarkus.container-image.build=true
```
Docker push:
```shell script
./mvnw clean package -Dquarkus.container-image.push=true
```

## Статический анализ

mvn spotbugs:spotbugs.
Результат анализа - target/spotbugsXml.xml 


